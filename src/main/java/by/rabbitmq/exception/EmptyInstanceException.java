package by.rabbitmq.exception;

public class EmptyInstanceException extends RuntimeException {
    public EmptyInstanceException(String message) {
        super(message);
    }
}
