package by.rabbitmq;

import by.rabbitmq.receiver.SocketReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReceiveRunner implements CommandLineRunner{
    private Logger logger = LoggerFactory.getLogger(ReceiveRunner.class);
    public static void main(String[] args) {
        SpringApplication.run(ReceiveRunner.class, args);
    }

    @Override
    public void run(String... strings)  {
        logger.info("The configuration is complete. Application is active.");
    }
}
