package by.rabbitmq.handlers;

import by.rabbitmq.bean.Contact;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.util.ContactKeeper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class EmailHandler implements WeatherDataEntityHandle {
    private ContactKeeper contactKeeper;
    private WeatherDAO weatherDAO;
    @Override
    public void handle(WeatherDataEntity weatherDataEntity) {
        List<Contact> contacts = weatherDAO.extractSubscriberEmail(weatherDataEntity);
        contactKeeper.addContacts(contacts);
    }

    public ContactKeeper getContactKeeper() {
        return contactKeeper;
    }

    public void setContactKeeper(ContactKeeper contactKeeper) {
        this.contactKeeper = contactKeeper;
    }

    public WeatherDAO getWeatherDAO() {
        return weatherDAO;
    }

    public void setWeatherDAO(WeatherDAO weatherDAO) {
        this.weatherDAO = weatherDAO;
    }
}
