package by.rabbitmq.handlers;

import by.rabbitmq.bean.Contact;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.receiver.SocketReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.List;

public class SocketHandler implements WeatherDataEntityHandle {
    private SimpMessagingTemplate template;
    private WeatherDAO weatherDAO;
    private Logger logger = LoggerFactory.getLogger(SocketReceiver.class);
    @Value("${websocket.stomp.endpoint}")
    private String endpoint;
    @Override
    public void handle(WeatherDataEntity weatherDataEntity) {
        List<Contact> contacts = weatherDAO.extractSubscriberEmail(weatherDataEntity);
        for (Contact contact: contacts) {
            try {
                template.convertAndSend(endpoint + contact.getEmail(), contact.getValue());
            } catch (MessagingException e) {
                logger.warn(e.getMessage());
            }
        }
    }

    public SimpMessagingTemplate getTemplate() {
        return template;
    }

    public void setTemplate(SimpMessagingTemplate template) {
        this.template = template;
    }

    public WeatherDAO getWeatherDAO() {
        return weatherDAO;
    }

    public void setWeatherDAO(WeatherDAO weatherDAO) {
        this.weatherDAO = weatherDAO;
    }
}
