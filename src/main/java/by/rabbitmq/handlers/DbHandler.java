package by.rabbitmq.handlers;

import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.local.DbAddressHolder;
import by.rabbitmq.local.DbTableName;
import by.rabbitmq.util.TableNameBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class DbHandler implements WeatherDataEntityHandle {
    private DbTableName dbTableName;
    private WeatherDAO weatherDAO;
    @Override
    public void handle(WeatherDataEntity weatherDataEntity) {
        DbAddressHolder.set(TableNameBuilder.buildFullTableName(weatherDataEntity, dbTableName.getTableName()));
        weatherDAO.create(weatherDataEntity);
    }

    public DbTableName getDbTableName() {
        return dbTableName;
    }

    public void setDbTableName(DbTableName dbTableName) {
        this.dbTableName = dbTableName;
    }

    public WeatherDAO getWeatherDAO() {
        return weatherDAO;
    }

    public void setWeatherDAO(WeatherDAO weatherDAO) {
        this.weatherDAO = weatherDAO;
    }
}
