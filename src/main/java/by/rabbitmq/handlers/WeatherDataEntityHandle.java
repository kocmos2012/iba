package by.rabbitmq.handlers;

import by.rabbitmq.bean.WeatherDataEntity;

public interface WeatherDataEntityHandle {
    void handle(WeatherDataEntity weatherDataEntity);
}
