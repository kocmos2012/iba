package by.rabbitmq.util;

import by.rabbitmq.bean.Contact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class ContactKeeper {

    private Logger logger = LoggerFactory.getLogger(ContactKeeper.class);
    private Queue<Contact> contacts = new ConcurrentLinkedQueue<>();
    private MailSender mailSender;
    private SimpleMailMessage templateMessage;

    public void addContacts(List<Contact> userData) {
        userData.forEach(x -> contacts.add(x));
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    @Scheduled (fixedRate = 3_600_000)
    private void sendMail() {
        SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
        Contact contact;
        while ((contact = contacts.poll()) != null) {
            msg.setTo(contact.getEmail());
            msg.setText(String.valueOf(contact.getValue()));
            try {
                this.mailSender.send(msg);
            } catch (MailException ex) {
                logger.warn(ex.getMessage());
            }
        }
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setTemplateMessage(SimpleMailMessage templateMessage) {
        this.templateMessage = templateMessage;
    }
}

