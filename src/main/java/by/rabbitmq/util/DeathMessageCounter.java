package by.rabbitmq.util;

import org.springframework.amqp.ImmediateAcknowledgeAmqpException;
import org.springframework.amqp.core.Message;

import java.util.List;
import java.util.Map;

/**
 * Check amqp.core.Message for resending of this message.
 */
public class DeathMessageCounter {
    private static final long REPETITION_LIMIT = 50;

    /**
     * Analyzes the message, checking if the number of resendings
     * has exceeded the specified constant value REPETITION_LIMIT.
     * @throws ImmediateAcknowledgeAmqpException if the message is
     * sent more than REPETITION_LIMIT times.
     */
    public static void checkMessageDeathCount(Message message) {
        String messageCount = "count";
        List<Map<String, ?>> xDeath = message.getMessageProperties().getXDeathHeader();
        if (xDeath != null) {
            xDeath.forEach(x -> {
                Long count = (Long)x.get(messageCount);
                if (count > REPETITION_LIMIT) {
                    throw new ImmediateAcknowledgeAmqpException("Request repetition count exhausted");
                }
            });
        }
    }
}
