package by.rabbitmq.util;

import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.exception.EmptyInstanceException;

public class TableNameBuilder {
    public static String buildFullTableName(WeatherDataEntity weatherDataEntity, String shortTableName) {
        if (weatherDataEntity == null) {
            throw new EmptyInstanceException("Can't concatenate " + shortTableName + " with null object");
        }
        return shortTableName + "_" + weatherDataEntity.getId();
    }
}
