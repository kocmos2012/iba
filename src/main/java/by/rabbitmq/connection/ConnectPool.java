package by.rabbitmq.connection;

import by.rabbitmq.exception.ConnectionPoolException;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectPool {
    @Value(value = "${postgresql.user}")
    private String user;
    @Value(value = "${postgresql.password}")
    private String password;
    @Value(value = "${postgresql.class}")
    private String className;
    @Value(value = "${postgresql.url}")
    private String url;
    @Value(value = "${postgresql.pool.timeout}")
    private int checkoutTimeout;

    private ComboPooledDataSource comboPooledDataSource;

    private ConnectPool() {
    }

    public Connection getConnection() throws SQLException{
        return comboPooledDataSource.getConnection();
    }

    @PreDestroy
    public void close(){
        comboPooledDataSource.hardReset();
        comboPooledDataSource.close();
    }

    @PostConstruct
    public void init() {
        try {
            comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource.setCheckoutTimeout(checkoutTimeout);
            comboPooledDataSource.setDriverClass(className);
            comboPooledDataSource.setJdbcUrl(url);
            comboPooledDataSource.setUser(user);
            comboPooledDataSource.setPassword(password);
            try (Connection connection = getConnection()){
            }
        } catch (PropertyVetoException | SQLException e) {
            throw new ConnectionPoolException(e);
        }
    }
}