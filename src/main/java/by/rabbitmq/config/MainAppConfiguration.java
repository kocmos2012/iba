package by.rabbitmq.config;

import org.springframework.amqp.rabbit.test.RabbitListenerTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Map;

@Configuration
//@ImportResource("classpath:socket_config.xml")
//@ImportResource("classpath:config.xml")
@ImportResource("classpath:email_config.xml")
@EnableScheduling
public class MainAppConfiguration {
    /*
    @Value("${rabbit.connection.hostname}")
    private String hostname;
    @Bean
    public MessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(hostname);
        com.rabbitmq.client.ConnectionFactory connectionFactory = cachingConnectionFactory.getRabbitConnectionFactory();
        MetricRegistry metricRegistry = new MetricRegistry();
        StandardMetricsCollector metrics = new StandardMetricsCollector(metricRegistry);
        connectionFactory.setMetricsCollector(metrics);
        connectionFactory.setRequestedHeartbeat(30);
        JmxReporter reporter = JmxReporter
                .forRegistry(metricRegistry)
                .inDomain("com.rabbitmq.client.jmx")
                .build();
        reporter.start();
        return cachingConnectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }*/
}
