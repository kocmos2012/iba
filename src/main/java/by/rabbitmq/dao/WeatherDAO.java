package by.rabbitmq.dao;



import by.rabbitmq.bean.Contact;
import by.rabbitmq.connection.ConnectPool;
import by.rabbitmq.exception.DAOException;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.local.DbAddressHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WeatherDAO implements IGenericDao<WeatherDataEntity,Contact> {
    private Logger logger = LoggerFactory.getLogger(WeatherDAO.class);
    @Autowired
    private ConnectPool connectPool;

    public void create(WeatherDataEntity entity) {
        StringBuilder sb = new StringBuilder("INSERT INTO ").append(DbAddressHolder.get())
                .append(" (attribute_id, value, time) VALUES (?,?,?)");
        try (Connection connection = connectPool.getConnection();
                PreparedStatement insertStatement = connection.prepareStatement(sb.toString())) {
            insertStatement.setLong(1, entity.getAttributeId());
            insertStatement.setDouble(2, entity.getValue());
            Timestamp timestamp = new Timestamp(entity.getCalendar().getTimeInMillis());
            insertStatement.setObject(3, timestamp);
            insertStatement.execute();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public WeatherDataEntity findOne(long id) {
        String getQuery =  "SELECT * FROM ? WHERE id = ?";
        try (Connection connection = connectPool.getConnection();
             PreparedStatement getStatement = connection.prepareStatement(getQuery)){
            getStatement.setString(1, DbAddressHolder.get());
            getStatement.setLong(2, id);
            ResultSet resultSet = getStatement.executeQuery();
            if(resultSet.next()) {
                long receivedId = resultSet.getLong(1);
                long attributeId = resultSet.getLong(2);
                double value = resultSet.getDouble(3);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(resultSet.getDate(4));
                return new WeatherDataEntity(receivedId, attributeId, value, calendar);
            } else {
                throw new DAOException("Object with id = " + id + " not find in the table " + DbAddressHolder.get());
            }
        } catch (SQLException e){
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public List<Contact> extractSubscriberEmail(WeatherDataEntity weatherDataEntity) {
        List<Contact> contacts = new ArrayList<>();
        double value = weatherDataEntity.getValue();
        String getContact = "SELECT subscriber.contact FROM subscriber \n" +
                "INNER JOIN subscription ON subscription.id = subscriber.subscription_id\n" +
                "INNER JOIN subscription_attributes " +
                "ON subscription_attributes.subscription_id = subscription.id\n" +
                "WHERE subscription_attributes.subscription_id = subscription.id AND\n" +
                "subscription.id = subscriber.subscription_id AND\n" +
                "subscription.station_id = ? AND\n" +
                "(subscription_attributes.minvalue > ? OR subscription_attributes.maxvalue < ?)";
        try (Connection connection = connectPool.getConnection();
             PreparedStatement getStatement = connection.prepareStatement(getContact)){
            getStatement.setLong(1, weatherDataEntity.getId());
            getStatement.setDouble(2, value);
            getStatement.setDouble(3, value);
            ResultSet resultSet = getStatement.executeQuery();
            String email;
            while (resultSet.next()) {
                try {
                    email = resultSet.getString(1).trim();
                    contacts.add(new Contact(email, value));
                } catch (SQLException e) {
                    logger.warn(e.getMessage());
                }
            }
            return contacts;
        } catch (SQLException e){
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public List<WeatherDataEntity> findAll() {
        throw new DAOException("Current method not support");
    }

    @Override
    public void update(WeatherDataEntity entity) {
        throw new DAOException("Current method not support");
    }

    @Override
    public void delete(WeatherDataEntity entity) {
        throw new DAOException("Current method not support");
    }

    @Override
    public void deleteById(long entityId) {
        throw new DAOException("Current method not support");
    }
}
