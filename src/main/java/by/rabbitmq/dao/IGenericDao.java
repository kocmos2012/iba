package by.rabbitmq.dao;

import java.util.List;

public interface IGenericDao<T,V> {

    T findOne(final long id);

    List<T> findAll();

    void create(final T entity);

    void update(final T entity);

    void delete(final T entity);

    void deleteById(final long entityId);

    List<V> extractSubscriberEmail(T entity);
}
