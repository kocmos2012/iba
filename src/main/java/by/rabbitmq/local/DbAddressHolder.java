package by.rabbitmq.local;

import org.springframework.stereotype.Component;

/**
 * Holds a unique string value for each thread.
 */
@Component
public class DbAddressHolder {
    private static final ThreadLocal<String> threadLocal = new  ThreadLocal<>();

    public static String get() {
        return threadLocal.get();
    }

    public static void set(String value) {
        threadLocal.set(value);
    }
}
