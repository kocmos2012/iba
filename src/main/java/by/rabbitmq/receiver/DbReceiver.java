package by.rabbitmq.receiver;

import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.handlers.WeatherDataEntityHandle;
import by.rabbitmq.local.DbTableName;
import by.rabbitmq.local.DbAddressHolder;
import by.rabbitmq.util.DeathMessageCounter;
import by.rabbitmq.util.TableNameBuilder;
import org.junit.Before;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;


public class DbReceiver implements MessageListener {
    private WeatherDataEntityHandle weatherDataEntityHandle;
    private MessageConverter jsonMessageConverter;

    public DbReceiver(WeatherDataEntityHandle weatherDataEntityHandle, MessageConverter jsonMessageConverter) {
        this.weatherDataEntityHandle = weatherDataEntityHandle;
        this.jsonMessageConverter = jsonMessageConverter;
    }

    public DbReceiver() {
    }

    @Override
    public void onMessage(Message message) {
        DeathMessageCounter.checkMessageDeathCount(message);
        WeatherDataEntity weatherDataEntity = (WeatherDataEntity) jsonMessageConverter.fromMessage(message);
        weatherDataEntityHandle.handle(weatherDataEntity);
    }

    public void setJsonMessageConverter(MessageConverter jsonMessageConverter) {
        this.jsonMessageConverter = jsonMessageConverter;
    }

    public MessageConverter getJsonMessageConverter() {
        return jsonMessageConverter;
    }

    public WeatherDataEntityHandle getWeatherDataEntityHandle() {
        return weatherDataEntityHandle;
    }

    public void setWeatherDataEntityHandle(WeatherDataEntityHandle weatherDataEntityHandle) {
        this.weatherDataEntityHandle = weatherDataEntityHandle;
    }
}

