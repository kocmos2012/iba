package by.rabbitmq.receiver;

import by.rabbitmq.bean.WeatherDataEntity;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.MessageConverter;


public class AbstractReceiver implements MessageListener {
    private MessageConverter jsonMessageConverter;

    @Override
    public void onMessage(Message message) {
        System.out.println("Socket Listener received message");
        WeatherDataEntity weatherDataEntity = (WeatherDataEntity) jsonMessageConverter.fromMessage(message);
    }

    public void setJsonMessageConverter(MessageConverter jsonMessageConverter) {
        this.jsonMessageConverter = jsonMessageConverter;
    }

    public MessageConverter getJsonMessageConverter() {
        return jsonMessageConverter;
    }

}
