package by.rabbitmq.receiver;

import by.rabbitmq.bean.Contact;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.handlers.WeatherDataEntityHandle;
import by.rabbitmq.util.ContactKeeper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class EmailReceiver implements MessageListener {
    private WeatherDataEntityHandle weatherDataEntityHandle;
    private MessageConverter jsonMessageConverter;

    public EmailReceiver(WeatherDataEntityHandle weatherDataEntityHandle, MessageConverter jsonMessageConverter) {
        this.weatherDataEntityHandle = weatherDataEntityHandle;
        this.jsonMessageConverter = jsonMessageConverter;
    }

    public EmailReceiver() {
    }

    @Override
    public void onMessage(Message message) {
        WeatherDataEntity weatherDataEntity = (WeatherDataEntity) jsonMessageConverter.fromMessage(message);
        weatherDataEntityHandle.handle(weatherDataEntity);
    }

    public void setJsonMessageConverter(MessageConverter jsonMessageConverter) {
        this.jsonMessageConverter = jsonMessageConverter;
    }

    public MessageConverter getJsonMessageConverter() {
        return jsonMessageConverter;
    }

    public WeatherDataEntityHandle getWeatherDataEntityHandle() {
        return weatherDataEntityHandle;
    }

    public void setWeatherDataEntityHandle(WeatherDataEntityHandle weatherDataEntityHandle) {
        this.weatherDataEntityHandle = weatherDataEntityHandle;
    }
}
