package by.rabbitmq.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.web.socket.messaging.SessionConnectEvent;

/**
 * Not using now.
 */
public class SocketApplicationListener implements ApplicationListener {
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof SessionConnectEvent) {
            SessionConnectEvent sessionConnectEvent = (SessionConnectEvent) event;
        }
    }
}
