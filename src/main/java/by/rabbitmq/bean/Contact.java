package by.rabbitmq.bean;

public class Contact {
    private final String email;
    private final double value;

    public Contact(String email, double value) {
        this.email = email;
        this.value = value;
    }

    public String getEmail() {
        return email;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "email='" + email + '\'' +
                ", value=" + value +
                '}';
    }
}
