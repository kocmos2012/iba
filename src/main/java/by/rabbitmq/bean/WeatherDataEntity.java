package by.rabbitmq.bean;

import org.json.JSONException;
import org.json.JSONObject;import java.util.Calendar;
import java.util.Objects;

public class WeatherDataEntity {
    private long id;
    private long attributeId;
    private Calendar calendar;
    private double value;

    public WeatherDataEntity(long id, long attributeId, double value, Calendar calendar) {
        this.id = id;
        this.attributeId = attributeId;
        this.calendar = calendar;
        this.value = value;
    }

    public WeatherDataEntity() {
    }

    public long getId() {
        return id;
    }

    public long getAttributeId() {
        return attributeId;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public double getValue() {
        return value;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAttributeId(long attributeId) {
        this.attributeId = attributeId;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("id", id);
            jsonData.put("attributeId", attributeId);
            jsonData.put("value", value);
            jsonData.put("calendar", calendar);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return jsonData.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherDataEntity that = (WeatherDataEntity) o;
        return id == that.id &&
                attributeId == that.attributeId &&
                Double.compare(that.value, value) == 0 &&
                Objects.equals(calendar, that.calendar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, attributeId, calendar, value);
    }
}
