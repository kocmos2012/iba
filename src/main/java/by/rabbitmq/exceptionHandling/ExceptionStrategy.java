package by.rabbitmq.exceptionHandling;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler.DefaultExceptionStrategy;

/**
 * Makes all user errors fatal.
 */
public class ExceptionStrategy extends DefaultExceptionStrategy {
    @Override
    protected boolean isUserCauseFatal(Throwable cause) {
        return true;
    }
}
