package by.rabbitmq.exceptionHandling;

import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.rabbit.listener.FatalExceptionStrategy;

public class ExceptionHandler extends ConditionalRejectingErrorHandler{
    public ExceptionHandler(FatalExceptionStrategy exceptionStrategy) {
        super(exceptionStrategy);
    }
}
