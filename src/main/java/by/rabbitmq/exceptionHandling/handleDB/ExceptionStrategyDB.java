package by.rabbitmq.exceptionHandling.handleDB;

import by.rabbitmq.exception.DAOException;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler.DefaultExceptionStrategy;

public class ExceptionStrategyDB extends DefaultExceptionStrategy {
    @Override
    protected boolean isUserCauseFatal(Throwable cause) {
        boolean result = true;
        if (cause instanceof DAOException) {
            result = false;
        }
        return result;
    }
}