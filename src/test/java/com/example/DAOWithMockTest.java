package com.example;

import by.rabbitmq.bean.Contact;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.connection.ConnectPool;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.exception.DAOException;
import by.rabbitmq.local.DbAddressHolder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.Calendar;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class DAOWithMockTest {
    @MockBean
    private ConnectPool connectPool;
    @MockBean
    private Connection mockConn;
    @MockBean
    private PreparedStatement mockPreparedStmnt;
    @MockBean
    private WeatherDataEntity weatherDataEntity;
    @MockBean
    private Calendar calendar;
    @MockBean
    private ResultSet resultSet;

    private WeatherDAO weatherDAO;
    private long attributeValue = 1L;
    private long IdValue = 2L;
    private double doubleValue = 1D;

    @Before
    public void init() throws SQLException {
        when(weatherDataEntity.getId()).thenReturn(IdValue);
        when(weatherDataEntity.getAttributeId()).thenReturn(attributeValue);
        when(weatherDataEntity.getValue()).thenReturn(doubleValue);
        when(weatherDataEntity.getCalendar()).thenReturn(calendar);
        when(connectPool.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmnt);
        when(mockPreparedStmnt.execute()).thenReturn(Boolean.TRUE);
        when(mockPreparedStmnt.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(resultSet.getString(1)).thenReturn("email");
        when(resultSet.getLong(1)).thenReturn(IdValue);
        when(resultSet.getLong(2)).thenReturn(attributeValue);
        when(resultSet.getDouble(3)).thenReturn(doubleValue);
        Class<?> clazz = WeatherDAO.class;
        try {
            weatherDAO = (WeatherDAO) clazz.newInstance();
            Field f1 = weatherDAO.getClass().getDeclaredField("connectPool");
            f1.setAccessible(true);
            ConnectPool oldPool = (ConnectPool) f1.get(weatherDAO);
            f1.set(weatherDAO, connectPool);
            ConnectPool pool = (ConnectPool) f1.get(weatherDAO);
            assertEquals(pool,connectPool);
            assertNotEquals(pool,oldPool);
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    @Test
    public void createTest() throws SQLException {
        try {
            weatherDAO.create(weatherDataEntity);
        } finally {
            verify(connectPool).getConnection();
            verify(mockConn).prepareStatement(anyString());
            verify(mockPreparedStmnt).setLong(1, attributeValue);
            verify(mockPreparedStmnt).setDouble(2, doubleValue);
            verify(mockPreparedStmnt).execute();
        }
    }

    @Test(expected = DAOException.class)
    public void createWithExceptionThrowingTest() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);
        try {
            weatherDAO.create(weatherDataEntity);
        } finally {
            verify(connectPool).getConnection();
            verify(mockConn, times(1)).prepareStatement(anyString());
            verify(mockPreparedStmnt, times(0)).setLong(1, attributeValue);
            verify(mockPreparedStmnt, times(0)).setDouble(2, doubleValue);
            verify(mockPreparedStmnt, times(0)).execute();
        }
    }

    @Test
    public void extractSubscriberEmailTest() throws SQLException {
        try {
            List<Contact> contacts = weatherDAO.extractSubscriberEmail(weatherDataEntity);
            assertThat(contacts.size(), is(1));
            assertThat( contacts.get(0).getEmail(), is("email"));
            assertThat(contacts.get(0).getValue(), is(doubleValue));
        } finally {
            verify(connectPool).getConnection();
            verify(mockConn).prepareStatement(anyString());
            verify(mockPreparedStmnt).setLong(1, IdValue);
            verify(mockPreparedStmnt).setDouble(2, doubleValue);
            verify(mockPreparedStmnt).setDouble(3, doubleValue);
            verify(mockPreparedStmnt).executeQuery();
            verify(resultSet,times(2)).next();
            verify(resultSet).getString(anyInt());
        }
    }

    @Test(expected = DAOException.class)
    public void extractSubscriberEmailWithExceptionThrowingTest() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);
        try {
            List<Contact> contacts = weatherDAO.extractSubscriberEmail(weatherDataEntity);
            assertThat(contacts.size(), is(0));
        } finally {
            verify(connectPool).getConnection();
            verify(mockConn).prepareStatement(anyString());
            verify(mockPreparedStmnt, times(0)).setLong(1, IdValue);
            verify(mockPreparedStmnt, times(0)).setDouble(2, doubleValue);
            verify(mockPreparedStmnt, times(0)).setDouble(3, doubleValue);
            verify(mockPreparedStmnt, times(0)).executeQuery();
            verify(resultSet, times(0)).next();
            verify(resultSet, times(0)).getString(anyInt());
        }
    }

    @Test
    public void findOneTest() throws SQLException {
        java.sql.Date date = new java.sql.Date(calendar.getTimeInMillis());
        when(resultSet.getDate(4)).thenReturn(date);
        try {
            DbAddressHolder.set("station");
            WeatherDataEntity resultEntity = weatherDAO.findOne(weatherDataEntity.getId());
            DbAddressHolder.set(null);
            assertThat(resultEntity.getId(),is(IdValue));
            assertThat(resultEntity.getAttributeId(),is(attributeValue));
            assertThat(resultEntity.getValue(),is(doubleValue));
            assertThat(resultEntity.getCalendar().getTimeInMillis(),is(calendar.getTimeInMillis()));
        } finally {
            verify(connectPool).getConnection();
            verify(mockConn).prepareStatement(anyString());
            verify(mockPreparedStmnt).setLong(2, IdValue);
            verify(mockPreparedStmnt).setString(anyInt(), anyString());
            verify(mockPreparedStmnt).executeQuery();
            verify(resultSet,times(2)).getLong(anyInt());
            verify(resultSet).getDouble(anyInt());
            verify(resultSet).getDate(anyInt());
        }
    }

    @Test(expected = DAOException.class)
    public void findOneWithExceptionThrowingTest() throws SQLException {
        java.sql.Date date = new java.sql.Date(calendar.getTimeInMillis());
        when(resultSet.getDate(4)).thenReturn(date);
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);
        try {
            DbAddressHolder.set("station");
            WeatherDataEntity resultEntity = weatherDAO.findOne(weatherDataEntity.getId());
            DbAddressHolder.set(null);
            assertNull(resultEntity);
        } finally {
            verify(connectPool).getConnection();
            verify(mockConn).prepareStatement(anyString());
            verify(mockPreparedStmnt,times(0)).setLong(2, IdValue);
            verify(mockPreparedStmnt,times(0)).setString(anyInt(), anyString());
            verify(mockPreparedStmnt,times(0)).executeQuery();
            verify(resultSet,times(0)).getLong(anyInt());
            verify(resultSet,times(0)).getDouble(anyInt());
            verify(resultSet,times(0)).getDate(anyInt());
        }
    }
}
