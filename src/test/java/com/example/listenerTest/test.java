package com.example.listenerTest;

import by.rabbitmq.handlers.WeatherDataEntityHandle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.validation.Constraint;

import java.io.IOException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(loader= AnnotationConfigContextLoader.class)
@ContextConfiguration("classpath:email_config.xml")
public class test {

    @Autowired
    private TestRabbitTemplate template;

    @Autowired
    private Config config;

    @Autowired
    @Qualifier(value = "listener")
    private MessageListener messageListener;

    @MockBean
    private WeatherDataEntityHandle weatherDataEntityHandle;

    @Autowired
    org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer messageListenerContainer;

    @Before
    public void before() {
        messageListenerContainer.setMessageListener(messageListener);
    }

    @Value(value = "${}")

    @Test
    public void testSimpleSends() {

    }

    @Test
    public void testSendAndReceive() {
        assertThat(this.template.convertSendAndReceive("baz", "hello"), equalTo("baz:hello"));
    }

    @Configuration
    @EnableRabbit
    public static class Config {


    }

}
