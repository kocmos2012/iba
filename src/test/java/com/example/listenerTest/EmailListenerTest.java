package com.example.listenerTest;

import by.rabbitmq.bean.WeatherDataEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:email_config.xml")
public class EmailListenerTest {
/*
    @Autowired
    private MessageListener listener;

    @InjectMocks
    private WeatherDataEntityHandle weatherDataEntityHandle;

    @Autowired
    private org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer container;*/

    @Autowired
    private RabbitTemplate rabbitTemplate;
    private WeatherDataEntity weatherDataEntity = new WeatherDataEntity(
            1, 2, 3, Calendar.getInstance());

    @Value("${rabbit.main.exchanger.name}")
    private String exchangerName;

   /* @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }*/
    /*@Before
    public void prepareListener () throws NoSuchFieldException, IllegalAccessException {
        Field f1 = listener.getClass().getDeclaredField("weatherDataEntityHandle");
        f1.setAccessible(true);
        f1.set(listener, weatherDataEntityHandle);
    }*/

    @Test
    public void test() throws InterruptedException {
        System.out.println(exchangerName);
        rabbitTemplate.convertAndSend(exchangerName,"*.email.*",weatherDataEntity);
        Thread.sleep(100);
        Thread.sleep(100);
        //verify(weatherDataEntityHandle,times(1)).handle(weatherDataEntity);
    }
}
