package com.example;

import by.rabbitmq.local.DbAddressHolder;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DbAddressHolderTest {
    @Test
    public void testHolder() {
        String currentThreadValue = "current";
        String otherThreadValue = "other";
        DbAddressHolder.set(currentThreadValue);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                DbAddressHolder.set(otherThreadValue);
                assertThat(DbAddressHolder.get(), is(otherThreadValue));
            }).start();
        }
        assertThat(DbAddressHolder.get(),is(currentThreadValue));
    }
}
