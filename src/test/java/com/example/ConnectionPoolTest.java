package com.example;

import by.rabbitmq.connection.ConnectPool;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:email_config.xml")
public class ConnectionPoolTest {
    @Autowired
    private ConnectPool connectPool;
    @Test
    public void test() throws SQLException {
        try (Connection connection = connectPool.getConnection()) {
           assertFalse(connection.isClosed());
        }
    }
}
