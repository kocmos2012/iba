package com.example.handlers;

import by.rabbitmq.bean.Contact;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.exception.DAOException;
import by.rabbitmq.handlers.SocketHandler;
import by.rabbitmq.handlers.WeatherDataEntityHandle;
import by.rabbitmq.receiver.SocketReceiver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
public class SocketHandlerTest {
    private SocketHandler socketHandler;
    @MockBean
    private WeatherDAO weatherDAO;
    @MockBean
    private SimpMessagingTemplate template;
    @MockBean
    private WeatherDataEntity weatherDataEntity;
    private List<Contact> resultList = new ArrayList<>(Arrays.asList(
            new Contact("vasiaEmail", 2.0),
            new Contact("koliaEmail", 3.0)));

    @Before
    public void init() {
        socketHandler = new SocketHandler();
        socketHandler.setTemplate(template);
        socketHandler.setWeatherDAO(weatherDAO);
    }

    @Test
    public void test() {
        when(weatherDAO.extractSubscriberEmail(weatherDataEntity)).thenReturn(resultList);
        socketHandler.handle(weatherDataEntity);
        verify(template,times(2)).convertAndSend(anyString(),anyDouble());
    }

    @Test(expected = DAOException.class)
    public void test2() {
        when(weatherDAO.extractSubscriberEmail(weatherDataEntity)).thenThrow(DAOException.class);
        try {
            socketHandler.handle(weatherDataEntity);
        } finally {
            verify(template, times(0)).convertAndSend(anyString(), anyDouble());
        }
    }
}
