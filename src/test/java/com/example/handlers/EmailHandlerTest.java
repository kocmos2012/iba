package com.example.handlers;

import by.rabbitmq.bean.Contact;
import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.exception.DAOException;
import by.rabbitmq.handlers.EmailHandler;
import by.rabbitmq.util.ContactKeeper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
public class EmailHandlerTest {
    private EmailHandler emailHandler;
    @MockBean
    private WeatherDAO weatherDAO;
    @MockBean
    private ContactKeeper contactKeeper;
    @MockBean
    private WeatherDataEntity weatherDataEntity;
    @MockBean
    private List<Contact> resultList;

    @Before
    public void init() {
        emailHandler = new EmailHandler();
        emailHandler.setWeatherDAO(weatherDAO);
        emailHandler.setContactKeeper(contactKeeper);
    }

    @Test
    public void test() {
        when(weatherDAO.extractSubscriberEmail(weatherDataEntity)).thenReturn(resultList);
        emailHandler.handle(weatherDataEntity);
        verify(contactKeeper).addContacts(resultList);
    }

    @Test(expected = DAOException.class)
    public void test2() {
        when(weatherDAO.extractSubscriberEmail(weatherDataEntity)).thenThrow(DAOException.class);
        try {
            emailHandler.handle(weatherDataEntity);
        } finally {
            verify(contactKeeper, times(0)).addContacts(resultList);
        }
    }
}
