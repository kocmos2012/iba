package com.example.handlers;



import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.dao.WeatherDAO;
import by.rabbitmq.exception.DAOException;
import by.rabbitmq.handlers.DbHandler;
import by.rabbitmq.local.DbTableName;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class DbHandlerTest {
    private DbHandler dbHandler;
    @MockBean
    private WeatherDAO weatherDAO;
    @MockBean
    private DbTableName dbTableName;
    @MockBean
    private WeatherDataEntity weatherDataEntity;

    @Before
    public void init() {
        dbHandler = new DbHandler();
        dbHandler.setDbTableName(dbTableName);
        dbHandler.setWeatherDAO(weatherDAO);
    }

    @Test
    public void test() {
        when(dbTableName.getTableName()).thenReturn("station");
        dbHandler.handle(weatherDataEntity);
        verify(weatherDAO).create(weatherDataEntity);
    }

    @Test(expected = DAOException.class)
    public void test2() {
        when(dbTableName.getTableName()).thenReturn("station");
        doThrow(DAOException.class).when(weatherDAO).create(weatherDataEntity);
        dbHandler.handle(weatherDataEntity);
    }
}
