package com.example;

import by.rabbitmq.util.DeathMessageCounter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.amqp.ImmediateAcknowledgeAmqpException;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.amqp.core.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  public static void checkMessageDeathCount(Message message) {
 String messageCount = "count";
 List<Map<String, ?>> xDeath = message.getMessageProperties().getXDeathHeader();
 if (xDeath != null) {
 xDeath.forEach(x -> {
 Long count = (Long)x.get(messageCount);
 if (count > REPETITION_LIMIT) {
 throw new ImmediateAcknowledgeAmqpException("Request repetition count exhausted");
 }
 });
 }
 }
 */
@RunWith(SpringRunner.class)
public class DeathMessageCounterTest {
    @MockBean
    private Message message;
    @MockBean
    private MessageProperties messageProperties;
    private List xDeath = new ArrayList();
    private Map XDeathHeader = new HashMap();

    @Before
    @SuppressWarnings("unchecked")
    public void initMock() {
        Mockito.when(message.getMessageProperties()).thenReturn(messageProperties);
        Mockito.when(messageProperties.getXDeathHeader()).thenReturn(xDeath);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void whenPutValueLessThen50ThenIsOk() {
        XDeathHeader.put("count", 49L);
        xDeath.add(XDeathHeader);
        DeathMessageCounter.checkMessageDeathCount(message);
    }

    @Test(expected = ImmediateAcknowledgeAmqpException.class)
    @SuppressWarnings("unchecked")
    public void whenPutValueGreatThen50ThenThrowException() {
        XDeathHeader.put("count", 55L);
        xDeath.add(XDeathHeader);
        DeathMessageCounter.checkMessageDeathCount(message);
    }

    @SuppressWarnings("unchecked")
    public void whenPutValueEqualThen50ThenIsOk() {
        XDeathHeader.put("count", 50L);
        xDeath.add(XDeathHeader);
        DeathMessageCounter.checkMessageDeathCount(message);
    }


}
