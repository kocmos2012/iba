package com.example;

import by.rabbitmq.bean.WeatherDataEntity;
import by.rabbitmq.exception.EmptyInstanceException;
import by.rabbitmq.util.TableNameBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
public class TableNameBuilderTest {

    @MockBean
    private WeatherDataEntity weatherDataEntity;
    private final String tableNamePart = "station";


    @Test
    public void whenGetCorrectObjectThenGetCorrectResult(){
        long lastTableNamePart = 100L;
        Mockito.when(weatherDataEntity.getId())
                .thenReturn(lastTableNamePart);
        assertEquals(TableNameBuilder.buildFullTableName(weatherDataEntity, tableNamePart),
                tableNamePart + "_" + lastTableNamePart);

    }

    @Test(expected = EmptyInstanceException.class)
    public void whenGetNullObjectThenThrowException(){
        TableNameBuilder.buildFullTableName(null, tableNamePart);
    }
}
