#binding.properties example

# Rabbit main queue, exchanger names.
rabbit.main.queue.name=mai

rabbit.main.exchanger.name=exchanger1

rabbit.connection.hostname=localhost

# Rabbit second queue, exchanger names.
# Need only for db configuration.
rabbit.second.queue.name=second2

rabbit.second.exchanger.name=exchanger2

rabbit.second.exchanger.messageReturnInterval=10000

# Db parameters for connection pool.
postgresql.class=org.postgresql.Driver

postgresql.url=jdbc:postgresql://localhost:5432/weather

postgresql.pool.timeout=3000

postgresql.user=postgres

postgresql.password=1234

postgresql.table.name=message1

# Mail sending configuration
email.username=mr.denejkin@yandex.ru

email.password=

email.subject=HELLO

email.hostname=smtp.gmail.com

email.port=587

#Socket
websocket.stomp.endpoint=/hi
websocket.destination.prefix=/arr